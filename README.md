# Helm chart para la plataforma IoK8S

Para instalarlo desde local:

```
helm install my-release iok8s/
```

con el nombre de la _release_ que se quiera utilizar. Por ejemplo `iok8s-example`.

Para instalarlo desde el registro de gitlab:

```
export HELM_EXPERIMENTAL_OCI=1
helm chart pull registry.gitlab.com/iok8s/charts/iok8s:v0.1.0
helm chart export registry.gitlab.com/iok8s/charts/iok8s:v0.1.0 -d /tmp/
helm upgrade --install my-release /tmp/iok8s
```

de nuevo, con la _release_ que se quiera utilizar. Se puede cambiar la versión del chart a las disponibles: [charts/tags](https://gitlab.com/iok8s/charts/-/tags).

Es necesario exportar la variable de entorno `HELM_EXPERIMENTAL_OCI` ya que `helm chart pull` es una funcionalidad experimental aún.